# libsubotto

Libreria python contenente i moduli principali che espongono i metodi necessari a gestire una 24ore.

 * `data.py` si interfaccia con il DB tramite SQLAlchemy
 * `core.py` fornisce i metodi per aggiungere eventi e in generale i metodi per i client
 * `listener.py` esporta la classe delle statistiche per aggregare i dati delle 24ore passate

Il database è specificato tramite la stringa di connessione sqlalchemy presente nel file `database_url` o nella variabile d'ambiente `SUBOTTO_DB_URL`.

## Installazione

Dare il comando `python setup.py install` oppure `pip install git+https://gitlab.com/subottosns/libsubotto.git`

## Developement

Installare la libreria con `python setup.py develop` oppure `pip install -e .`
