#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""This module exports the SubottoCore class, which is the unique interface to the DB"""

import sys
import datetime

from .data import Session, Team, Player, Match, PlayerMatch, Event, QueueElement


def act_init_match(name, team_a_name, team_b_name, sched_begin, sched_end, year=None):
    """Add a match to DB with given teams, begn and end"""
    session = Session()
    match = Match()
    match.sched_begin = sched_begin
    match.sched_end = sched_end
    match.begin = None
    match.end = None
    match.name = name
    match.year = year

    team_a = session.query(Team).filter(Team.name == team_a_name).one()
    team_b = session.query(Team).filter(Team.name == team_b_name).one()
    match.team_a = team_a
    match.team_b = team_b
    session.add(match)
    session.commit()
    return match.id


class SubottoCore:
    """Class that interfaces clients (self.listeners) with the database.
    It must be the only place where events are sent and read."""

    def __init__(self, match_id):
        self.session = Session()
        self.match = self.session.query(Match).filter(Match.id == match_id).one()
        self.teams = [self.match.team_a, self.match.team_b]
        self.order = [None, None]
        self.queues = [[], []]
        self.score = [0, 0]
        self.prev_partial = [0, 0]
        self.partial = [0, 0]
        self.players = [[None, None], [None, None]]
        self.listeners = []

        self.last_event_id = 0
        self.last_player_match_id = 0
        self.last_timestamp = None
        self.last_goal_time = None

    def close(self):
        self.session.rollback()
        self.session.close()

    def detect_team(self, team):
        if team == self.match.team_a:
            return 0
        else:
            return 1

    def new_player_match(self, player_match):
        print("> Received new player match: {}".format(player_match), file=sys.stderr)

        for listener in self.listeners:
            listener.new_player_match(player_match)

    def new_event(self, event):
        """New event coming from the database: updates internal status and send notification to listeners"""

        print("> Received new event: {}".format(event), file=sys.stderr)

        if event.type == Event.EV_TYPE_SWAP:
            self.order = [event.red_team, event.blue_team]

        elif event.type == Event.EV_TYPE_CHANGE:
            self.players[self.detect_team(event.team)] = [event.player_a, event.player_b]
            self.prev_partial = self.partial if self.partial != [0, 0] else self.prev_partial
            self.partial = [0, 0]

        elif event.type == Event.EV_TYPE_GOAL:
            self.last_goal_time = event.timestamp
            self.score[self.detect_team(event.team)] += 1
            self.partial[self.detect_team(event.team)] += 1

        elif event.type == Event.EV_TYPE_GOAL_UNDO:
            self.score[self.detect_team(event.team)] -= 1
            if self.partial[self.detect_team(event.team)] > 0:
                self.partial[self.detect_team(event.team)] -= 1
            else:
                self.prev_partial[self.detect_team(event.team)] -= 1

        elif event.type == Event.EV_TYPE_ADVANTAGE_PHASE:
            pass

        else:
            print("> Wrong event type {}".format(event.type), file=sys.stderr)

        for listener in self.listeners:
            listener.new_event(event)

    def regenerate(self):
        """Makes listeners rebuild after an update"""
        print("> Regeneration", file=sys.stderr)
        for listener in self.listeners:
            listener.regenerate()

    def update(self):
        """Checks for any update regarding the current match: player_matches, events, queues"""
        self.session.rollback()

        if self.match.begin is None:
            self.session.refresh(self.match)

        for player_match in self.session.query(PlayerMatch).filter(PlayerMatch.match == self.match).filter(PlayerMatch.id > self.last_player_match_id).order_by(PlayerMatch.id):
            self.new_player_match(player_match)
            self.last_player_match_id = player_match.id

        for event in self.session.query(Event).filter(Event.match == self.match).filter(Event.id > self.last_event_id).order_by(Event.id):
            if self.last_timestamp is not None and event.timestamp <= self.last_timestamp:
                print("> Timestamp monotonicity error at {}".format(event.timestamp), file=sys.stderr)
                # sys.exit(1)
            self.new_event(event)
            self.last_timestamp = event.timestamp
            self.last_event_id = event.id

        for idx in [0, 1]:
            team = self.teams[idx]
            this_num = 0
            self.queues[idx] = []
            for queue_element in self.match.get_queue(team):
                if queue_element.num != this_num:
                    print("> Error: queues are inconsistent", file=sys.stderr)
                this_num += 1
                self.queues[idx].append((queue_element.player_a, queue_element.player_b))

        self.regenerate()
        return True

    def act_event(self, event, source=None):
        """Adds an event to the database after some checks"""

        if self.match.begin is None and event.type in [Event.EV_TYPE_GOAL, Event.EV_TYPE_GOAL_UNDO]:
            print("> Match not yet started, rejecting goal...")
            return
        if event.type == Event.EV_TYPE_GOAL_UNDO and self.score[self.detect_team(event.team)] == 0:
            print("> Team at zero points, rejecting goal undo...")
            return

        ora = datetime.datetime.now()
        event.timestamp = ora
        event.match = self.match
        if source is None:
            event.source = Event.EV_SOURCE_MANUAL
        else:
            event.source = source

        if not event.check_type():
            # TODO: get specific error message
            print("> Received bad event, not sending to DB", file=sys.stderr)
            return
        self.session.add(event)
        self.session.commit()
        self.update()

    def act_switch_teams(self, source=None):
        e = Event()
        e.type = Event.EV_TYPE_SWAP
        if self.order == [None, None]:
            e.red_team = self.teams[0]
            e.blue_team = self.teams[1]
        else:
            e.red_team = self.order[1]
            e.blue_team = self.order[0]
        self.act_event(e, source)

    def act_goal(self, team, source=None):
        e = Event()
        e.type = Event.EV_TYPE_GOAL
        e.team = team
        self.act_event(e, source)

    def act_goal_undo(self, team, source=None):
        e = Event()
        e.type = Event.EV_TYPE_GOAL_UNDO
        e.team = team
        self.act_event(e, source)

    def act_team_change(self, team, player_a, player_b, source=None):
        e = Event()
        e.type = Event.EV_TYPE_CHANGE
        e.team = team
        e.player_a = player_a
        e.player_b = player_b
        self.act_event(e, source)

    def act_add_to_queue(self, team, player_a, player_b):
        """Adds players to team's queue"""
        qe = QueueElement()
        qe.match = self.match
        qe.team = team
        qe.player_a = player_a
        qe.player_b = player_b
        qe.num = len(self.queues[self.detect_team(team)])
        self.session.add(qe)
        self.session.commit()
        self.update()

    def act_remove_from_queue(self, team, num):
        """Remove queue element from queue, and renumber the others"""
        queue = self.match.get_queue(team)
        self.session.delete(queue[num])
        self.session.flush()
        del queue[num]
        # Indirect assignement to prevent failing constraints
        for i in range(num, len(queue)):
            queue[i].num = None
        self.session.flush()
        for i in range(num, len(queue)):
            queue[i].num = i
        self.session.commit()
        self.update()

    def act_swap_queue(self, team, num1, num2):
        """Swap two queues elements"""
        queue = self.match.get_queue(team)
        # Indirect assignement to prevent failing constraints
        queue[num1].num = None
        queue[num2].num = None
        self.session.flush()
        queue[num1].num = num2
        queue[num2].num = num1
        self.session.commit()
        self.update()

    def act_begin_match(self, begin=None):
        if begin is None:
            begin = datetime.datetime.now()
        self.match.begin = begin
        self.session.commit()
        print("> Started match {} at {}".format(self.match.id, begin), file=sys.stderr)

    def act_end_match(self, end=None):
        if end is None:
            end = datetime.datetime.now()
        self.match.end = end
        self.session.commit()
        print("> Ended match {} at {}".format(self.match.id, end), file=sys.stderr)

    def act_add_player_match_from_name(self, team, fname, lname, comment=None, bulk=False):
        player = Player.get_or_create(self.session, fname, lname, comment)
        player_match = PlayerMatch.get_or_create(self.session, player, self.match, team)
        return player

    #def get_player_from_id(self, id):
        #player = Player.get_from_id(self.session, id)
        #return player

    def pull_all_players(self):
        list = Player.pull_all(self.session)
        return list

    def easy_get_red_team(self):
        return self.order[0]

    def easy_get_blue_team(self):
        return self.order[1]

    def easy_get_red_score(self):
        return self.score[self.detect_team(self.easy_get_red_team())]

    def easy_get_blue_score(self):
        return self.score[self.detect_team(self.easy_get_blue_team())]

    def easy_get_red_part(self):
        return self.partial[self.detect_team(self.easy_get_red_team())]

    def easy_get_blue_part(self):
        return self.partial[self.detect_team(self.easy_get_blue_team())]

    def easy_get_red_prev_part(self):
        return self.prev_partial[self.detect_team(self.easy_get_red_team())]

    def easy_get_blue_prev_part(self):
        return self.prev_partial[self.detect_team(self.easy_get_blue_team())]

    def easy_act_red_goal_cell(self):
        self.act_goal(self.easy_get_blue_team(), source=Event.EV_SOURCE_CELL_RED_PLAIN)

    def easy_act_red_supergoal_cell(self):
        self.act_goal(self.easy_get_blue_team(), source=Event.EV_SOURCE_CELL_RED_SUPER)

    def easy_act_red_goal_button(self):
        self.act_goal(self.easy_get_red_team(), source=Event.EV_SOURCE_BUTTON_RED_GOAL)

    def easy_act_red_goalundo_button(self):
        self.act_goal_undo(self.easy_get_red_team(), source=Event.EV_SOURCE_BUTTON_RED_UNDO)

    def easy_act_blue_goal_cell(self):
        self.act_goal(self.easy_get_red_team(), source=Event.EV_SOURCE_CELL_BLUE_PLAIN)

    def easy_act_blue_supergoal_cell(self):
        self.act_goal(self.easy_get_red_team(), source=Event.EV_SOURCE_CELL_BLUE_SUPER)

    def easy_act_blue_goal_button(self):
        self.act_goal(self.easy_get_blue_team(), source=Event.EV_SOURCE_BUTTON_BLUE_GOAL)

    def easy_act_blue_goalundo_button(self):
        self.act_goal_undo(self.easy_get_blue_team(), source=Event.EV_SOURCE_BUTTON_BLUE_UNDO)

    def is_match_ongoing(self):
        self.session.refresh(self.match)
        return self.match.begin is not None and self.match.end is None
