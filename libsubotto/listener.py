#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""This module exports the Statistics class"""

import sys
import datetime

from .data import Event, Match, Player, StatsPlayerMatch
from .core import SubottoCore

INTERESTING_SCORES = [42, 100, 250, 500, 750, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000]


def format_player(player):
    return "%s %s" % (player.fname, player.lname)


def get_status(begin, phase, end):
    if begin is None:
        return "before"
    elif end is not None:
        return "ended"
    elif phase is None:
        return "running"
    else:
        return "advantage"


def get_estimated_score(score, elapsed, length):
    if elapsed is None or elapsed <= 0.1:
        return [None, None]
    elif length - elapsed <= 0.1:
        return score
    else:
        return [int(s*length/elapsed) for s in score]


def get_remount_index(score, elapsed, length):
    if elapsed is None:
        return None

    to_go = length - elapsed
    if to_go <= 0.0:
        return "Infinity"
    else:
        return float(abs(score[0] - score[1])) / to_go * 60.0 * 60.0


def get_winning_team(teams, score):
    if score[0] > score[1]:
        return teams[0].name
    elif score[0] < score[1]:
        return teams[1].name
    else:
        return None


def get_losing_team(teams, score):
    if score[0] > score[1]:
        return teams[1].name
    elif score[0] < score[1]:
        return teams[0].name
    else:
        return None


def get_interesting_score(score):
    idx = list(map(lambda x: x > score, INTERESTING_SCORES)).index(True)
    return INTERESTING_SCORES[idx]


def get_linear_projection(score, target, elapsed, begin):
    if begin is None or elapsed is None or elapsed <= 0.1:
        return None
    if score == 0:
        return "Infinity"

    ratio = float(score) / elapsed
    return (begin + datetime.timedelta(seconds=float(target)/ratio)).strftime("%H:%M:%S")


class Statistics:
    """Class that computes current match statistics"""

    def __init__(self, match_id, old_matches_id):
        self.core = SubottoCore(match_id)
        self.core.listeners.append(self)

        self.old_matches = self.core.session.query(Match).filter(Match.id.in_(old_matches_id)).all()
        players = self.core.session.query(Player).all()
        old_stats_player_matches = self.core.session.query(StatsPlayerMatch).filter(StatsPlayerMatch.match_id.in_(old_matches_id)).all()

        self.current_phase = None

        self.total_goals = dict([])    # Map from player.id to the total number of goals ever
        self.cur_goals = dict([])    # Map from player.id to the number of goals in this 24-hours tournament
        self.total_time = dict([])    # Map from player.id to the total number of seconds played ever
        self.cur_time = dict([])    # Map from player.id to the number of seconds played in this 24-hours tournament
        self.participations = dict([])    # Map from player.id to the number of 24-hours played

        # Informazioni sui singoli giocatori...
        for player in players:
            self.total_goals[player.id] = 0
            self.cur_goals[player.id] = 0
            self.total_time[player.id] = datetime.timedelta(0, 0, 0)
            self.cur_time[player.id] = datetime.timedelta(0, 0, 0)

        self.turn_begin = None  # Beginning of last turn (in the current match)

        # Team id => Stack of the sequence of player id
        self.goal_sequence = {self.core.match.team_a_id: [], self.core.match.team_b_id: []}

        # Team id => Pair of player id
        self.current_contestants = {self.core.match.team_a_id: [], self.core.match.team_b_id: []}

        # Team id => Time of last change
        self.last_change = {self.core.match.team_a_id: None, self.core.match.team_b_id: None}

        for stats_player_match in old_stats_player_matches:
            self.total_time[stats_player_match.player_id] += datetime.timedelta(seconds=stats_player_match.seconds)
            self.total_goals[stats_player_match.player_id] += stats_player_match.pos_goals
            if stats_player_match.player_id not in self.participations:
                self.participations[stats_player_match.player_id] = dict()
            self.participations[stats_player_match.player_id][stats_player_match.match_id] = 1

    def new_event(self, event):
        print(">> Received new event: {}".format(event), file=sys.stderr)

        if event.type == Event.EV_TYPE_SWAP:
            pass

        elif event.type == Event.EV_TYPE_CHANGE:
            # Aggiorno i tempi di gioco dei giocatori uscenti
            team_id = event.team_id
            timestamp = event.timestamp

            if self.last_change[team_id] is not None:
                # Aggiorno il tempo di gioco dei giocatori che stanno uscendo
                delta_time = timestamp - self.last_change[team_id]

                for player_id in [self.current_contestants[team_id][0], self.current_contestants[team_id][1]]:
                    self.total_time[player_id] += delta_time
                    self.cur_time[player_id] += delta_time

            # Effettuo il cambio
            self.current_contestants[team_id] = [event.player_a_id, event.player_b_id]
            if self.last_change[team_id] is None:
                self.last_change[team_id] = self.core.match.begin
                self.turn_begin = self.core.match.begin
            else:
                self.last_change[team_id] = timestamp
                self.turn_begin = timestamp

        elif event.type == Event.EV_TYPE_GOAL:
            # Aggiornamento statistiche
            team_id = event.team_id
            contestants = self.current_contestants[team_id]

            for c in contestants:
                self.total_goals[c] += 1
                self.cur_goals[c] += 1

            self.goal_sequence[team_id].append(contestants)

        elif event.type == Event.EV_TYPE_GOAL_UNDO:
            # Aggiorno le statistiche: tolgo l'ultimo goal di goal_sequence
            team_id = event.team_id
            contestants = self.goal_sequence[team_id].pop()

            for c in contestants:
                self.total_goals[c] -= 1
                self.cur_goals[c] -= 1

        elif event.type == Event.EV_TYPE_ADVANTAGE_PHASE:
            self.current_phase = event.phase

    def new_player_match(self, player_match):
        print(">> Received new player match: {player_match}", file=sys.stderr)

        # TODO: verificare (sperare) che questa funzione venga chiamata solo se quel player_match non esisteva ancora
        if player_match.player_id not in self.participations:
            self.participations[player_match.player_id] = dict()
            self.total_goals[player_match.player_id] = 0
            self.cur_goals[player_match.player_id] = 0
            self.total_time[player_match.player_id] = datetime.timedelta(0, 0, 0)
            self.cur_time[player_match.player_id] = datetime.timedelta(0, 0, 0)
        self.participations[player_match.player_id][player_match.match_id] = 1

    def regenerate(self):
        """Compute current status and save it to dictionary"""
        print(">> Generate current data", file=sys.stderr)

        # Compute time-related data
        now = datetime.datetime.now()
        if self.core.match.begin is not None:
            # La partita è già iniziata
            elapsed_time = (now - self.core.match.begin).total_seconds()
            time_to_begin = None
            match_begin = None
        else:
            # La partita deve ancora iniziare
            elapsed_time = None
            time_to_begin = (self.core.match.sched_begin - now).total_seconds()
            match_begin = (self.core.match.sched_begin - datetime.datetime(1970, 1, 1)).total_seconds()

        if self.core.match.end is not None:
            # La partita, oltre che essere iniziata, è anche finita.
            elapsed_time = (self.core.match.end - self.core.match.begin).total_seconds()
            time_to_end = None
        else:
            # La partita non è ancora finita
            time_to_end = (self.core.match.sched_end - now).total_seconds()

        if elapsed_time is not None and elapsed_time > 0.01:
            goals_per_minute = float(self.core.score[0] + self.core.score[1]) * 60.0 / elapsed_time
        else:
            goals_per_minute = None

        # Compute team-related data
        # 0 => color of team_a, 1 => color of team_b
        if self.core.match.begin is not None and self.core.order[0] is not None:
            if self.core.teams[0] == self.core.order[0]:
                colors = ['red', 'blue']
            else:
                colors = ['blue', 'red']
        else:
            colors = [None, None]

        # Compute turn_duration
        turn_end = now
        if self.core.match.end is not None:
            turn_end = self.core.match.end

        if self.turn_begin is None and self.core.match.begin is not None:
            self.turn_begin = self.core.match.begin
        if self.turn_begin is not None:
            turn_duration = turn_end - self.turn_begin
        else:
            turn_duration = datetime.timedelta()

        # Compute estimation-related data
        length = (self.core.match.sched_end - self.core.match.sched_begin).total_seconds()
        estimated_score = get_estimated_score(self.core.score, elapsed_time, length)
        remount_index = get_remount_index(self.core.score, elapsed_time, length)
        interesting_score = [get_interesting_score(s) for s in self.core.score]
        linear_projection = [get_linear_projection(self.core.score[i], interesting_score[i], elapsed_time, self.core.match.begin) for i in range(2)]

        # Create data dictionary
        data = {
            'status': get_status(self.core.match.begin, self.current_phase, self.core.match.end),
            'time_to_begin': time_to_begin,
            'time_to_end': time_to_end,
            'match_begin': match_begin,
            'elapsed_time': elapsed_time,
            'teams': {
                self.core.teams[i].id: {
                    'id': self.core.teams[i].id,
                    'name': self.core.teams[i].name,
                    'score': self.core.score[i],
                    'partial_score': self.core.partial[i],
                    'prev_partial_score': self.core.prev_partial[i],
                    'estimated_score': estimated_score[i],
                    'color': colors[i],
                    'interesting_score': interesting_score[i],
                    'linear_projection': linear_projection[i],
                    'players': [
                        {
                            'id': self.core.players[i][j].id,
                            'fname': self.core.players[i][j].fname,
                            'lname': self.core.players[i][j].lname,
                            'total_time': (self.total_time[self.core.players[i][j].id] + turn_duration).total_seconds(),
                            'played_time': (self.cur_time[self.core.players[i][j].id] + turn_duration).total_seconds(),
                            'total_goals': self.total_goals[self.core.players[i][j].id],
                            'num_goals': self.cur_goals[self.core.players[i][j].id],
                            'participations': sum(self.participations[self.core.players[i][j].id].values()),
                        }
                    for j in range(2)] if self.core.players[i] != [None, None] else None,
                    'queue': [
                        {
                            'player_a': {
                                'id': q_el.player_a.id,
                                'fname': q_el.player_a.fname,
                                'lname': q_el.player_a.lname,
                            },
                            'player_b': {
                                'id': q_el.player_b.id,
                                'fname': q_el.player_b.fname,
                                'lname': q_el.player_b.lname,
                            }
                        }
                    for q_el in self.core.match.get_queue(self.core.teams[i])]
                }
                for i in range(2)},
            'goal_difference': abs(self.core.score[0] - self.core.score[1]),
            'total_goals': self.core.score[0] + self.core.score[1],
            'goals_per_minute': goals_per_minute,
            'turn_duration': turn_duration.total_seconds(),
            'remount_index': remount_index,
            }

        self.data = data
