#!/usr/bin/env python

from distutils.core import setup

setup(name='libsubotto',
      version='1.0.1',
      author='Subotto Technical Team',
      description='Subotto library for 24ore matches',
      url='https://gitlab.com/subottosns/libsubotto',
      packages=['libsubotto'],
      install_requires=['sqlalchemy==1.3.17']
      )
